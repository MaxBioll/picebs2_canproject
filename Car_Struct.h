/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef XC_HEADER_TEMPLATE_H
#define	XC_HEADER_TEMPLATE_H 

#include <xc.h>
#include <stdint.h>        /* For uint8_t definition */
typedef struct
    {
        uint8_t id;                 //ID of the car
        uint8_t tempomatState;      //0 -> no tempomate, 1 -> tempomat activated
        uint8_t tempomatSpeed;      //The tempomat speed
        uint8_t gearSel;            //The value P,R,N or D
        uint16_t frontSensReq;      //Indication of an obstacle in front of the car
        uint8_t extSensorLeft;      //Indication of an obstacle in front left
        uint8_t extSensorRight;     //Indication of an obstacle in front right
        uint16_t motorRPM;          //RPM of the motor
        uint16_t motorSpeed;        //Speed of the car
        uint8_t brakePedal;         //Brake pedal position
        uint8_t accelPedal;         //Accelerator pedal position
        uint8_t contactKey;         //State of the contact key
        uint8_t steeringWReq;       //Steering wheel position
        uint8_t brokenCar;          //Broken value indication code
        uint8_t slopeReq;           //Car slop value (-100/front down to 100/front up)
        uint8_t gearLVL;            //The gear level (0 to 5)
        uint8_t meter;
        uint8_t hour;
        uint8_t minute;
        uint8_t second;
        uint8_t power;
        uint8_t manySpeed;
    } CAR_STRUCT;
    
#endif	/* XC_HEADER_TEMPLATE_H */

