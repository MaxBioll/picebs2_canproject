/*
 * File:   messages.c
 * Author: maxim
 *
 * Created on 29. mai 2020, 12:54
 */


#include "messages.h"
#include "picebs2_canlib/can.h"
#include "Car_Struct.h"

extern CAR_STRUCT myCar;

void Light_Front(uint8_t intensity){
    struct CANMESSAGE txMsg;
    txMsg.extended_identifier = 0;
    txMsg.identifier = ((0x11 << 4)| myCar.id);
    txMsg.dta[0] = intensity;
    for(int i = 0; i < 7; i++){
        txMsg.dta[i + 1] = 0;
    }
    txMsg.dlc = 1;
    txMsg.rtr = 0;
    txMsg.txPrio = 1;
    Can_PutMessage(&txMsg);
}

void Light_Back(uint8_t intensity){
    struct CANMESSAGE txMsg;
    txMsg.extended_identifier = 0;
    txMsg.identifier = ((0x12 << 4)| myCar.id);
    txMsg.dta[0] = intensity;
    for(int i = 0; i < 7; i++){
        txMsg.dta[i + 1] = 0;
    }
    txMsg.dlc = 1;
    txMsg.rtr = 0;
    txMsg.txPrio = 1;
    Can_PutMessage(&txMsg);
}

void Time(uint8_t hour, uint8_t minutes, uint8_t colon){
    struct CANMESSAGE txMsg;
    txMsg.extended_identifier = 0;
    txMsg.identifier = ((0x13 << 4)| myCar.id);
    txMsg.dta[0] = hour;
    txMsg.dta[1] = minutes;
    txMsg.dta[2] = colon;
    for(int i = 0; i < 5; i++){
        txMsg.dta[i + 3] = 0;
    }
    txMsg.dlc = 3;
    txMsg.rtr = 0;
    txMsg.txPrio = 1;
    Can_PutMessage(&txMsg);
}
    //////////
    //A tester
    //////////
void Gear_Lvl(uint8_t gearLvl){
    struct CANMESSAGE txMsg;
    txMsg.extended_identifier = 0;
    txMsg.identifier = ((0x14 << 4)| myCar.id);
    txMsg.dta[0] = gearLvl;
    for(int i = 0; i < 7; i++){
        txMsg.dta[i + 1] = 0;
    }
    txMsg.dlc = 1;
    txMsg.rtr = 0;
    txMsg.txPrio = 1;
    Can_PutMessage(&txMsg);
}

void Audio(uint8_t volume, uint8_t wheels){
    struct CANMESSAGE txMsg;
    txMsg.extended_identifier = 0;
    txMsg.identifier = ((0x15 << 4)| myCar.id);
    txMsg.dta[0] = volume;
    txMsg.dta[1] = wheels;
    for(int i = 0; i < 6; i++){
        txMsg.dta[i + 2] = 0;
    }
    txMsg.dlc = 2;
    txMsg.rtr = 0;
    txMsg.txPrio = 1;
    Can_PutMessage(&txMsg);
}

void Pwr_Motor(uint8_t power){
    struct CANMESSAGE txMsg;
    txMsg.extended_identifier = 0;
    txMsg.identifier = ((0x16 << 4)| myCar.id);
    txMsg.dta[0] = power;
    for(int i = 0; i < 7; i++){
        txMsg.dta[i + 1] = 0;
    }
    txMsg.dlc = 1;
    txMsg.rtr = 0;
    txMsg.txPrio = 1;
    Can_PutMessage(&txMsg);
}

void Pwr_Brake(uint8_t power){
    struct CANMESSAGE txMsg;
    txMsg.extended_identifier = 0;
    txMsg.identifier = ((0x17 << 4)| myCar.id);
    txMsg.dta[0] = power;
    for(int i = 0; i < 7; i++){
        txMsg.dta[i + 1] = 0;
    }
    txMsg.dlc = 1;
    txMsg.rtr = 0;
    txMsg.txPrio = 1;
    Can_PutMessage(&txMsg);
}

void Tempo_Off(){
    struct CANMESSAGE txMsg;
    txMsg.extended_identifier = 0;
    txMsg.identifier = ((0x18 << 4)| myCar.id);
    for(int i = 0; i < 8; i++){
        txMsg.dta[i] = 0;
    }
    txMsg.dlc = 0;
    txMsg.rtr = 0;
    txMsg.txPrio = 1;
    Can_PutMessage(&txMsg);
}

void Km_Pulse(){
    struct CANMESSAGE txMsg;
    txMsg.extended_identifier = 0;
    txMsg.identifier = ((0x19 << 4)| myCar.id);
    for(int i = 0; i < 8; i++){
        txMsg.dta[i] = 0;
    }
    txMsg.dlc = 0;
    txMsg.rtr = 0;
    txMsg.txPrio = 1;
    Can_PutMessage(&txMsg);
}

void Car_Rst(){
    struct CANMESSAGE txMsg;
    txMsg.extended_identifier = 0;
    txMsg.identifier = ((0x1F << 4)| myCar.id);
    for(int i = 0; i < 8; i++){
        txMsg.dta[i] = 0;
    }
    txMsg.dlc = 0;
    txMsg.rtr = 0;
    txMsg.txPrio = 1;
    Can_PutMessage(&txMsg);
}

