#include <pic18.h>
#include <stdint.h>

void Light_Front(uint8_t intensity);

void Light_Back(uint8_t intensity);

void Time(uint8_t hour, uint8_t minutes, uint8_t colon);

void Gear_Lvl(uint8_t gearLvl);

void Audio(uint8_t volume, uint8_t wheels);

void Pwr_Motor(uint8_t power);

void Pwr_Brake(uint8_t power);

void Tempo_Off();

void Km_Pulse();

void Auto_Steering(uint8_t steeringPos, uint8_t autoControl); //Don't use cause we don't programm race mode

void Car_Rst();