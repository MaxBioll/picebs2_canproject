/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#include <stdlib.h>
#include "Car_Struct.h"
#include "messages.h"

#if defined(__XC)
    #include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#endif
#define _XTAL_FREQ 64000000L

#include "picebs2_canlib/can.h"
#include "picebs2_lcdlib/lcd_highlevel.h"

/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/

CAR_STRUCT myCar;

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

/*Define the messages CAN ID from car*/
#define TEMPOMAT 0x01
#define GEAR_SEL 0x02
#define FRONT_SENS_REQ 0x03
#define EXT_SENSORS 0x04
#define MOTOR_STATUS 0x05
#define BRAKE_PEDAL 0x06
#define ACCEL_PEDAL 0x07
#define CONTACT_KEY 0x08
#define STEERING_W_REQ 0x09
#define BROKEN_CAR 0x0A
#define BAD_MESSAGE 0x0B
#define SLOPE_REQ 0x0C
#define RACE 0x0D
#define CAR_ID 0X0F
   
/*Define the messages CAN ID from the external controller*/
#define LIGHT_FRONT 0x11
#define LIGHT_BACK 0x12
#define TIME 0x13
#define GEAR_LVL 0X14
#define AUDIO 0x15
#define PWR_MOTOR 0x16
#define PWR_BRAKE 0x17
#define TEMPO_OFF 0x18
#define KM_PULSE 0x19
#define AUTO_STEERING 0x1A
#define CAR_RST 0x1F


void CarControl()
{
    //if the contact key isn't turned on
    if(myCar.contactKey == 0)
    {
        myCar.manySpeed = 0;
        Audio(0, 0);
        //The car is in park mode
        Tempo_Off();
        Light_Front(0);
        Light_Back(0);
        myCar.gearSel = 'P';
    }else if(myCar.motorRPM < 800){
        Pwr_Motor(10);
    }
    if(myCar.contactKey == 1){
        Light_Front(100);
        Light_Back(50);
        
        Pwr_Brake(myCar.brakePedal); 
        
        if(myCar.gearLVL > 0){
            Audio(myCar.power, 1);
        }else{
            Audio(myCar.power, 0);
        }
        
        if(myCar.brakePedal > 10){
            Light_Back(100);
            Tempo_Off();
        }
         if(myCar.tempomatState == 0)
        {
             if(myCar.manySpeed == 0){
                 myCar.power = myCar.accelPedal*90/100+10;
             }else{
                 myCar.power -= 10;
             }
             
             Pwr_Motor(myCar.power);
         } else{
         if(myCar.motorSpeed > myCar.tempomatSpeed + 4){
                 myCar.power--;
             }else if(myCar.motorSpeed < myCar.tempomatSpeed -4){
                 myCar.power++;
             }
             Pwr_Motor(myCar.power);
         }
    }
}

void Change_Gear(){
    switch(myCar.gearSel)
    {
        //The car is in neutral mode
        case 'N':
            //The wheels of the car are free
            Tempo_Off();
            Gear_Lvl(0);
            myCar.gearLVL = 0;
            if(myCar.motorRPM > 6000){
                myCar.manySpeed = 1;
            }else{
                myCar.manySpeed = 0;
            }
            break;
           
        //The car is in reverse mode
        case 'R':
            //Only the gear 1 can be used
            Tempo_Off();
            switch(myCar.gearLVL){
                case 0:
                    if(myCar.motorRPM > 2000){
                        myCar.gearLVL = 1; 
                    }
                    break;
                case 1:
                    if(myCar.motorRPM < 2000){
                        myCar.gearLVL = 0; 
                    }else if(myCar.motorRPM > 6000){
                        myCar.manySpeed = 1;
                    }else{
                        myCar.manySpeed = 0;
                    }
                    break;
            }
            Gear_Lvl(myCar.gearLVL);
            break;
          
        //The car is in drive mode
        case 'D':
            switch (myCar.gearLVL){
                case 5:
                    if(myCar.motorRPM < 2500){
                        Gear_Lvl(4);
                        myCar.gearLVL = 4;
                    } else if(myCar.motorRPM > 7000 | myCar.motorSpeed > 180){
                        myCar.manySpeed = 1;
                    }else{
                        myCar.manySpeed = 0;
                    }
                    break;
                case 0:
                    if(myCar.motorRPM > 2000){
                        Gear_Lvl(1);
                        myCar.gearLVL = 1;
                    }
                    break;
                default:
                    if(myCar.motorRPM < 2000){
                        myCar.gearLVL--;
                        Gear_Lvl(myCar.gearLVL);
                    }else if(myCar.motorRPM > 6000){
                        myCar.gearLVL++;
                        Gear_Lvl(myCar.gearLVL);
                    }
                    break;
                }

            break;
        //The car is in park mode
        case 'P':
            Tempo_Off();
            Gear_Lvl(0);
            myCar.gearLVL = 0;
             if(myCar.contactKey == 1)
            {
                 myCar.power = 10;
                 if(myCar.motorRPM > 6000){
                        myCar.manySpeed = 1;
                    }else{
                        myCar.manySpeed = 0;
                    }
            }
            else{
            //The car is blocked
                 myCar.power = 0;
            }
            Pwr_Motor(myCar.power);
            break;
    }
}
   

void main(void)
{
      //Interrupts Init
  GIE = 1;
  INTEDG1 = 0;
  INT1IF = 0;
  INT1IE = 1;
    
    TMR0ON = 1;
    T0CS = 0;
    PSA = 0;                
    T0CONbits.T0PS = 7;
    TMR0IE = 1;
    TMR0IF = 0;
    T08BIT = 0;             
    TMR0 = 3036;
    
    myCar.hour = 0;
    myCar.minute = 0;
    myCar.second = 0;
    myCar.power = 0;
    myCar.meter = 0;
    myCar.manySpeed = 0;
    
    struct CANFILTER selFilters;
    struct CANMESSAGE txMsg,rxMsg;
    /* Configure the oscillator for the device works @ 64MHz*/
   PLLEN = 1;            // activate PLL x4
   OSCCON = 0b01110000;  // for 64MHz cpu clock (default is 8MHz)
    // Caution -> the PLL needs up to 2 [ms] to start !
    __delay_ms(2); 
    /* Initialize I/O and Peripherals for application */
  selFilters.mask0 = 0x7FF;    // all bits are filtered (filter0 & filter1)
  selFilters.mask1 = 0x7FF;    // all bits are filtered (filter2 to filter5)
  selFilters.filter0 = 0xFF;  // filter address 0x7FF
  selFilters.filter1 = 0xFF;    // filter address 0x01
  selFilters.filter2 = 0xFF;  // filter address 0xAA
  selFilters.filter3 = 0xFF;  // filter address 0x00
  selFilters.filter4 = 0xFF;  // filter address 0x00
  selFilters.filter5 = 0xFF;  // filter address 0x00
  selFilters.ext0 = 0;          // standard identifiers used for M0  
  selFilters.ext1 = 0;          // standard identifiers used for M1  
  Can_Init(&selFilters);        // example of call 

  txMsg.identifier = 0xFF;
  txMsg.dlc = 0;
  txMsg.rtr = 1;
  txMsg.extended_identifier = 0;
  txMsg.txPrio = 1;
  for(int i = 0; i < 8; i++){
      txMsg.dta[i]= 0;
  }
  Can_PutMessage(&txMsg);
  while(Can_GetMessage(&rxMsg) != CAN_OK){}
  myCar.id = rxMsg.dta[0];
  selFilters.mask0 = 0x0F;    // all bits are filtered (filter0 & filter1)
  selFilters.mask1 = 0x0F;    // all bits are filtered (filter2 to filter5)
  selFilters.filter0 = myCar.id;  // filter address 0x7FF
  selFilters.filter1 = myCar.id;    // filter address 0x01
  selFilters.filter2 = myCar.id;  // filter address 0xAA
  selFilters.filter3 = myCar.id;  // filter address 0x00
  selFilters.filter4 = myCar.id;  // filter address 0x00
  selFilters.filter5 = myCar.id;  // filter address 0x00
  selFilters.ext0 = 0;          // standard identifiers used for M0  
  selFilters.ext1 = 0;          // standard identifiers used for M1  
  Can_Init(&selFilters);        // example of call 
  
    while(1)
    {
        
        
        if(Can_GetMessage(&rxMsg) == CAN_OK)
        {
            switch (rxMsg.identifier >> 4)
            {
                case 0x08:
                    myCar.contactKey = rxMsg.dta[0];
                    break;
                case 0x07:
                    myCar.accelPedal = rxMsg.dta[0];
                    break;
                case 0x06:
                    myCar.brakePedal = rxMsg.dta[0];
                    break;
                case 0x02:
                    myCar.gearSel = rxMsg.dta[0];
                    break;
                case 0x04:
                    myCar.motorRPM = (rxMsg.dta[0] << 8) | rxMsg.dta[1]; 
                    myCar.motorSpeed = (rxMsg.dta[2] << 8) | rxMsg.dta[3]; 
                    Change_Gear();
                    break;
                case 0x0A:
                    myCar.brokenCar = rxMsg.dta[0];
                    break;
                case 0x01:
                    myCar.tempomatState = rxMsg.dta[0];
                    myCar.tempomatSpeed = rxMsg.dta[1];
                    break;
            }
        }
        CarControl();
    }
}

