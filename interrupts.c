/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#include "picebs2_canlib/can.h"
#include "messages.h"
#include "Car_Struct.h"

#endif

extern CAR_STRUCT myCar;
/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/
/* High-priority service */

void interrupt high_isr(void)
{
  if((CAN_INTF == 1)&&(CAN_INTE == 1)) // interrupt flag & active
  {
    CAN_INTF = 0;               // clear interrupt
    Can_Isr();                  // interrupt treatment
    if(CAN_INTPIN == 0)         // check pin is high again
    {
      CAN_INTF = 1;             // no -> re-create interrupt
    }
  }
  
  if((TMR0IF == 1)&&(TMR0IE == 1))
  {
      TMR0IF = 0;
      
      ///////////
      //HOURS
      ///////////
      if(myCar.second < 59){
          myCar.second++;
      }else{
          myCar.second = 0;
          if(myCar.minute < 59 ){
              myCar.minute++;
          }else{
              myCar.minute = 0;
              if(myCar.hour < 23){
                  myCar.hour++;
              }else{
                  myCar.hour = 0;
              }
          }
      }
      Time(myCar.hour, myCar.minute, myCar.second % 2);
     
      myCar.meter += (uint8_t)(myCar.motorSpeed *5 / 18);
      while(myCar.meter > 100){
          myCar.meter -= 100;
          Km_Pulse();
      }
      
      
      
      TMR0 = 3036; //load next interrupt (25ms)
  }
}

